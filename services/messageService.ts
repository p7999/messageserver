const Vonage = require("@vonage/server-sdk");
const moment = require('moment-timezone');

const vonage = new Vonage({
  apiKey: process.env.API_KEY,
  apiSecret: process.env.API_SECRET,
});

const from = "PayGov";
const opts = {
  type: "unicode",
};

const formatDate = (date) => {
  return moment.tz(date, 'Asia/Jerusalem').format('DD/MM/YYYY');
}

const createMessageBy = (message) => {
  const messageTemplate = {
    1: `בוקר טוב ${message.first_name + ' ' + message.last_name}!\n
    התווסף לך חשבון חדש של ${message.organization_name} לתשלום.\n
    לתשלום מהיר ונוח יש להיכנס לאתר ${process.env.APP_URL}\n`,
    2: `בוקר טוב ${message.first_name + ' ' + message.last_name} !\n
    קיים חשבון של ${message.organization_name} לתשלום מהתאריך ${formatDate(message.creation_date)} שלא שולם עדיין.\n
    כדי שלא תצטרך לשלם קנס ניתן לשלם בצורה מהירה ובטוחה דרך האתר ${process.env.APP_URL}\n`,
    3: `בוקר טוב ${message.first_name + ' ' + message.last_name} !\n
    עבר הרבה זמן מאז שקיבלת את החשבון של ${message.organization_name}.\n
    כדי שלא תצטרך לשלם קנס ניתן לשלם בצורה מהירה ובטוחה דרך האתר ${process.env.APP_URL}\n`
  }

  return messageTemplate[message.status];
};

const sendMessage = (bill) => {
  let message = createMessageBy(bill);

  sendSMS(bill.phone_number, message);
};

const sendSMS = (to, message) => {
  vonage.message.sendSms(from, to, message, opts, (err, responseData) => {
    if (err) {
      console.log(err);
    } else {
      if (responseData.messages[0]["status"] === "0") {
        console.log("Message sent successfully.");
      } else {
        console.log(
          `Message failed with error: ${responseData.messages[0]["error-text"]}`
        );
      }
    }
  });
};

export {sendMessage};
