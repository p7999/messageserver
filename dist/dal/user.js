"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllUsers = void 0;
const getAllUsers = (sql) => __awaiter(void 0, void 0, void 0, function* () {
    // const users =  await sql.query `SELECT [id], [description] 
    // FROM [PayGov].[dbo].[status]`;
    // return users.recordset;
    return [
        {
            "bank_account_number": "",
            "bank_branch_number": "",
            "city": "Rishon Lezion",
            "email": "avi@paygov.com",
            "name": "ירדן",
            "ID": "313131334",
            "phone_number": "+972545894468"
        },
        {
            "bank_account_number": "",
            "bank_branch_number": "",
            "city": "Rishon Lezion",
            "email": "avi@paygov.com",
            "name": "קורל",
            "ID": "313131336",
            "phone_number": "+972502421839"
        }
    ];
});
exports.getAllUsers = getAllUsers;
//# sourceMappingURL=user.js.map