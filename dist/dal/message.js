"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllMessagesForSend = exports.updateMessage = exports.insertMessage = void 0;
const mssql_1 = __importDefault(require("mssql"));
const insertMessage = (pool, messageDetails) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `INSERT INTO [PayGov].[dbo].[message] (user_id, type, status, creation_date) 
    VALUES (@user_id,@type,@status,GETDATE());`;
    yield pool.request().
        input('user_id', mssql_1.default.text, messageDetails.ID).
        input('type', mssql_1.default.BigInt, 1).
        input('status', mssql_1.default.BigInt, 1)
        .query(query);
});
exports.insertMessage = insertMessage;
const updateMessage = (pool, messageDetails) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `INSERT INTO messages (user_id, type, status, creation_date) 
    VALUES (@user_id,@type,@status,@creation_date);`;
    yield pool.request().
        input('user_id', mssql_1.default.BigInt, messageDetails.user_id).
        input('type', mssql_1.default.BigInt, messageDetails.type).
        input('status', mssql_1.default.BigInt, 1).
        input('creation_date', mssql_1.default.Time, Date.now())
        .query(query);
});
exports.updateMessage = updateMessage;
const getAllMessagesForSend = (pool, user_id) => __awaiter(void 0, void 0, void 0, function* () {
    const query = `INSERT INTO messages (user_id, type, status, creation_date) 
    VALUES (@user_id,@type,@status,@creation_date);`;
    yield pool.request()
        .query(query);
});
exports.getAllMessagesForSend = getAllMessagesForSend;
//# sourceMappingURL=message.js.map