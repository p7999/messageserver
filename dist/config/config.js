"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.initConfig = void 0;
const sqlConfig = {
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    server: process.env.DB_SERVER,
    pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 30000
    },
    options: {
        trustServerCertificate: true
    }
};
const vonageConfig = {
    apiKey: process.env.API_KEY,
    apiSecret: process.env.API_SECRET
};
const initConfig = () => {
    return {
        sqlConfig: sqlConfig,
        vonageConfig: vonageConfig
    };
};
exports.initConfig = initConfig;
//# sourceMappingURL=config.js.map