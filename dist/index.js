"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
require('dotenv').config();
const mssql_1 = __importDefault(require("mssql"));
const config_1 = require("./config/config");
const user_1 = require("./dal/user");
const message_1 = require("./dal/message");
const app = (0, express_1.default)();
let pool;
const PORT = process.env.PORT || 3389;
app.listen(PORT, () => {
    console.log(`PayGov server listening on port ${PORT}`);
});
app.get('/messageServer', (req, res) => {
    res.send("Rapi is child");
});
const initDBPool = (config) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        pool = yield mssql_1.default.connect(config);
    }
    catch (error) {
        console.error(`Error connecting to DB, error=` + error);
    }
});
const getUpdatesFromAPI = (userID) => {
    const updates = [
        {
            "customer": "313131331",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131332",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131333",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131334",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131335",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131336",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        },
        {
            "customer": "313131331",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
        }
    ];
    return updates.filter((update) => update.customer === userID);
};
const checkForUpdates = () => __awaiter(void 0, void 0, void 0, function* () {
    const users = yield (0, user_1.getAllUsers)(mssql_1.default);
    users.map((user) => {
        console.log("User: " + user.ID);
        let bills = getUpdatesFromAPI(user.ID);
        bills.map((bill) => {
            console.log("Bill: " + JSON.stringify(bill));
            bill = Object.assign(Object.assign({}, user), bill);
            (0, message_1.insertMessage)(pool, bill);
        });
        console.log("-----------------------");
    });
});
const updatesCheck = () => __awaiter(void 0, void 0, void 0, function* () {
    const appConfigs = (0, config_1.initConfig)();
    yield initDBPool(appConfigs.sqlConfig);
    yield checkForUpdates();
});
//setInterval(updatesCheck, 10000);
//# sourceMappingURL=index.js.map