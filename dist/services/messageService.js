"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMessage = void 0;
const Vonage = require("@vonage/server-sdk");
const vonage = new Vonage({
    apiKey: process.env.API_KEY,
    apiSecret: process.env.API_SECRET,
});
const from = "PayGov";
const opts = {
    type: "unicode",
};
const createMessageBy = (userName) => {
    const text = `היי ${userName} :)\n יש לך חשבון חדש לתשלום.\n לתשלום בקלות היכנס ${process.env.APP_URL}\n`;
    return text;
};
const sendMessage = (bill) => {
    let message = createMessageBy(bill.userName);
    sendSMS(bill.phone_number, message);
};
exports.sendMessage = sendMessage;
const sendSMS = (to, message) => {
    vonage.message.sendSms(from, to, message, opts, (err, responseData) => {
        if (err) {
            console.log(err);
        }
        else {
            if (responseData.messages[0]["status"] === "0") {
                console.log("Message sent successfully.");
            }
            else {
                console.log(`Message failed with error: ${responseData.messages[0]["error-text"]}`);
            }
        }
    });
};
//# sourceMappingURL=messageService.js.map