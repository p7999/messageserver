import sql from 'mssql';

const insertMessage = async (pool, messageDetails) => {
    const query = `INSERT INTO [PayGov].[dbo].[message] (user_id, type, status, creation_date, update_date) 
    VALUES (@user_id,@type,@status,GETDATE(),GETDATE());`;
    try {
        await pool.request().
        input('user_id', sql.Text, messageDetails.user_id.toString()).
        input('type', sql.BigInt, messageDetails.organizationID).
        input('status', sql.BigInt, 1)
        .query(query, (err, result) => {
            console.log("InsertMessage Result: " + JSON.stringify(result));
        });
    } catch(error) {
        console.error(error);
    }
    
}

const updateMessage = async (pool, messageDetails) => {
    const query = `UPDATE [PayGov].[dbo].[message]
                    SET [status] = @status
                    ,[update_date] = GETDATE()
                     WHERE [USER_ID] like @user_id and [type] = @type`;
    try {             
        await pool.request().
        input('user_id', sql.Text, messageDetails.user_id.toString()).
        input('type', sql.BigInt, messageDetails.organization_id).
        input('status', sql.BigInt, (parseInt(messageDetails.status) + 1).toString())
        .query(query, (err, result) => {
            console.log("UpdateMessage Result: " + err?JSON.stringify(err) : JSON.stringify(result));
        });
    } catch(error) {
        console.error(error);
    }
}

const getAllMessagesForSend = async (pool, user_id, status) => {
    const query = `SELECT [id], [user_id], [type], [status], [creation_date], [update_date]
                    FROM [PayGov].[dbo].[message]
                    WHERE [user_id] like @user_id and [status] = @status`;
    try {
        return await pool.request().
        input('user_id', sql.Text, user_id).
        input('status', sql.BigInt, status)
        .query(query);
    } catch(error) {
        console.error(error);
    }
}

export {insertMessage, updateMessage, getAllMessagesForSend}