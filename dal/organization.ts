export const getAllOrganizations = async(pool) => {
    return await pool.request().query(`SELECT [ID] as organization_id
                                            ,[Name] as organization_name
                                            ,[Email] as organization_email
                                            ,[Private_Key]
                                            ,[BaseApi]
                                        FROM [PayGov].[dbo].[Organizations]`);
};