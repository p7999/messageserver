export const getAllUsers = async(pool) => {
    return await pool.request().query(`SELECT [UserID] as user_id
                                            ,[FirstName] as first_name
                                            ,[LastName] as last_name
                                            ,[PhoneNumber] as phone_number
                                            ,[Address] as address
                                        FROM [PayGov].[dbo].[Users]`);
};