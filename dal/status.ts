const getAllstatuses = async(sql) => {
  const status =  await sql.query `SELECT [id], [description] 
  FROM [PayGov].[dbo].[status]`;
  return status.recordset;
};

module.exports = getAllstatuses;
