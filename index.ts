export {};
    import express from 'express';
require('dotenv').config();
import sql from 'mssql';
import { initConfig } from './config/config';
import { getAllUsers } from './dal/user';
import { insertMessage, updateMessage, getAllMessagesForSend } from './dal/message';
import { sendMessage } from './services/messageService';
import { getAllOrganizations } from './dal/organization';

const app = express();

let pool;

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`PayGov server listening on port ${PORT}`);
});

app.get(
    '/checkForUpdates', (req, res) => {
        checkForUpdates();
        res.send("checkForUpdates started");
    }
);

const initDBPool = async(config) => {
    try {
        pool = await sql.connect(config).catch(err => {
            console.error("Connection error: " + err);
            process.exit(1);
          });;
    } catch (error) {
        console.error(`Error connecting to DB, error=` + error);
    }
}

const getUpdatesFromAPI = (userID: string) => {
    const updates = [
        {
            "customer": "11111",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 1
        },
        {
            "customer": "313131332",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 1
        },
        {
            "customer": "313131333",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 1
        },
        {
            "customer": "313131334",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 1
        },
        {
            "customer": "11111",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 3
        },
        {
            "customer": "313131336",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 2
        },
        {
            "customer": "11111",
            "isPaid": "false",
            "paidDate": "",
            "receiveDate": "2022-03-29T023:35:20.382",
            "organizationID": 2
        }
    ];

    return updates.filter((update) => update.customer === userID);
}

const checkForUpdates = async () => {
    const users = await getAllUsers(pool);
    Promise.all(
    users.recordset.map((user) => {
        console.log("User: " + user.user_id.toString());
        let bills = getUpdatesFromAPI(user.user_id.toString());
        bills.map((bill) => {
            console.log("Bill: " + JSON.stringify(bill));
            bill = {... user, ... bill};
            //insertMessage(pool, bill);
        })
        console.log("-----------------------");
    }));
}

const sendUpdates = async () => {
    console.log("Send updates");
    await updatesCheck();
    const users = await getAllUsers(pool);
    Promise.all(
    users.recordset.map(async (user) => {
        console.log("User: " + user.user_id);
        let messages = await getAllMessagesForSend(pool, user.user_id.toString(), 2);
        let organizations = await getAllOrganizations(pool);
        Promise.all([messages, organizations]).then((data) => {
            messages = data[0].recordset;
            organizations = data[1].recordset;
            messages.map((message) => {
                let org = organizations.filter((org) => org.organization_id == message.type)[0];
                message = {... message, ... org, ... user};
                sendMessage(message);
                updateMessage(pool, message);
            })
          });

        console.log("-----------------------");
    }));
}

const updatesCheck = async() => {
    const appConfigs = initConfig();
    await initDBPool(appConfigs.sqlConfig);
    await checkForUpdates();
}

setInterval(async() => {
    let hour = new Date().getHours();
    if (hour == 23) {
        await updatesCheck();
    } else if (hour == 9) {
        await sendUpdates();
    }
}, 600000);